# � <-- This is here to make sure that the encoding stays ANSI, do not remove it
name = "Template Debug War"
war_goal = {
	type = take_claim
    	province=1
	casus_belli = cb_conquest
}

#Righteous A01 wages war against neigbors 
3044.6.1 = {
	add_attacker = A01
	add_defender = A02
	add_defender = A03
}
#Wins war very fast due to Jersayen aid
3044.7.1 = {
	rem_attacker = A01
	rem_defender = A02
	rem_defender = A03
}