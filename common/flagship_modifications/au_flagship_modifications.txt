# � <-- This is here to make sure that the encoding stays ANSI, do not remove it
place_the_holder = {
	trigger = {
		has_country_modifier = place_the_holder_boat
	}
	modifier = {
		flagship_durability = 1
		number_of_cannons_flagship_modifier = 20
		naval_maintenance_flagship_modifier = 1
		morale_in_fleet_modifier = 0.1
	}
	ai_trade_score = {
		factor = 0
    }
	ai_war_score = {
		factor = 10
	}
}

