natives_empty = { #this is also the default look
	graphical_culture = inuitgfx
	color = { 0 255 0 }
	icon = 1
	
	unit = place_holder_1_place_holder
	
	provinces = {
		#Testland
		9875 9876
		#Nova Placeholder
		1221 1222 1233 1234
}

natives_old_testland = {
	graphical_culture = northamericagfx
	color = { 0 255 0 }
	icon = 2
	
	unit = place_holder_1_place_holder
	
	provinces = {
		#Ruins of old Testland
		1110 1111
	}
}